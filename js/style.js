
let number1 = parseFloat(prompt("Введіть перше число:"));
let number2 = parseFloat(prompt("Введіть друге число:"));


let operator = prompt("Введіть математичну операцію (+, -, *, /):");


function calculate(num1, num2, oper) {
  switch(oper) {
    case '+':
      return num1 + num2;
    case '-':
      return num1 - num2;
    case '*':
      return num1 * num2;
    case '/':
      return num1 / num2;
    default:
      return NaN;
  }
}


let result = calculate(number1, number2, operator);
console.log(result);